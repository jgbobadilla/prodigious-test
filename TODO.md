Things to be done on this test

##Documentation
* Brief explaining solution and the reason of choices done.

##HTML/CSS
* Icons
* Improve contact field 

##Improvements
* Improve styles and GUI interaction
* Improve charts

##I18n
* Define and apply i18n 
* Toggle button
