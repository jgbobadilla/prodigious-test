# Gabriel Bobadilla's test for Prodigious README

##Modules
The proposed solution has the following set of four modules:

* Stock info module: One of the most complex, and driven by data. I planned to use a char library and color variations according to the behaviour of the stock. So, my CSS code reflects a little example in that way, with classes .

* Form: There are forms everywhere on websites, so it is important to know how to build them, so I included it. I was also interested into giving a try for that improved contact field. Unfortunately, time was not enough to provide a good solution, so came back to a simple strategy.

* Blog post: It is very pretty with those two images, and it is nice for applying on a wide set of sites. Can also be fed with data providing from JSON files.

* Ribbon module: I decided to develop it because seems simple, but has its challenge with the figure on the left. Also, I assume it is not frequently chosen.


##Layout and responsiveness
The modules are sorted in a 2x2 grid layout for big screens with a fixed width. When its natural size doesn´t fits well for medium screens, I set just a column and keep the fixed width. For smaller screens I get advantage of responsive images to create modules of adjustable width.

I want to remark that breakpoints were not defined for specific devices, but the ones required to give a nce experience in most of devices.

##CSS
CSS was created by using Sass pre-processor with a set of components that grouped intention of those styles: layouts, colors, mixins, generals and so on.

##JS
I used AJAX via jQuery for its simplicity of use.
