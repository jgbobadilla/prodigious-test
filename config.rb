# Set this to the root of your project when deployed:
css_dir = "css"
sass_dir = "scss"
output_style = :nested 
line_comments = true
preferred_syntax = :scss
