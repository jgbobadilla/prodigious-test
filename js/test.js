$(document).ready(function() {
  loadData();
});

function loadData() {

  //Send the asynchronous request
  $.getJSON("/data/config-modules.json")
    .done( function( json ) {
      if(json.widgets) {
        processData(json.widgets);
      } else {
        console.log( "Bad format data" );
      }
    })
    .fail( function( jqxhr, textStatus, error ) {
      console.log( "Request Failed: " + error );
    });
}

function processData(data) {
  if(data["stock-module"]) {
    updateStockData( data["stock-module"] );
  } else {
    console.log( "No stock widget data" );
  }

  if(data["blog-post"]) {
    updateBlogPost( data["blog-post"] );
  } else {
    console.log( "No blog post widget data" );
  }

  if(data["ribbon"]) {
    updateRibbon( data["ribbon"] );
  } else {
    console.log( "No ribbon widget data" );
  }
}

function updateStockData(data) {
  var $base = $('#stock-module');

  if($base.length !== 1) {
    return;
  }

  if(data.trend) {
    $('.number', $base).addClass(data.trend);
  }
  updateText($('.value', $base), data.value);
  updateText($('.change', $base), data.change);
  updateText($('.time', $base), data.date);
  updateText($('.city', $base), data.location);
  updateText($('.yearly-change .value', $base), data["yearlyChange"]);
  updateText($('.shares .amount', $base), data["sharesTraded"]);
  updateText($('.market-cap .amount', $base), data["marketCap"]);

  updateImage($('.stock-graph img', $base), data.graph);
  updateImage($('.aapl-charts img', $base), data["aapl-image"]);
}

function updateBlogPost(data) {
  var $base = $('#blog-post');

  updateImage($('.profile-image img', $base), data.profileImage);
  updateImage($('.avatar img', $base), data.profileAvatar);
  updateText($('.author-data .name', $base), data.authorName);
  updateText($('.author-data .description', $base), data.authorDescription);
  updateText($('.visits', $base), data.visits);
  updateText($('.comments', $base), data.comments);
  updateText($('.likes', $base), data.likes);
}

function updateRibbon(data) {
  var $base = $('#ribbon');

  if($base.length !== 1) {
    return;
  }

  updateText($('.discount', $base), data.discount + "%");
}

function updateText(field, value) {
  if(value) {
    field.text(value);
  }
}

function updateImage(field, value) {
  if(value) {
    field.attr('src', '/imgs/' + value);
  }
}
